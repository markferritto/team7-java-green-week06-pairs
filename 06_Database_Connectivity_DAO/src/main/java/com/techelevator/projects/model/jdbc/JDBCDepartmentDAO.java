package com.techelevator.projects.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.techelevator.projects.model.Department;
import com.techelevator.projects.model.DepartmentDAO;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class JDBCDepartmentDAO implements DepartmentDAO {

    private JdbcTemplate jdbcTemplate;

    public JDBCDepartmentDAO(DataSource dataSource) {

        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Department> getAllDepartments() {

        List<Department> allDepartments = new ArrayList<>();    //create empty list to hold results

        String sqlGetAllDepartments = "SELECT department_id, name " +    //write our query
                "FROM department";

        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllDepartments);    //executing the query and getting back a list of records

        while (results.next()) {    //loop through all records

            Department department = mapRowToDepartment(results);    //map each record to a department object
            allDepartments.add(department);    //add the department object to the list

        }

        return allDepartments;
    }

    private Department mapRowToDepartment(SqlRowSet results) {
        Department department = new Department();

        department.setId(results.getLong("department_id"));
        department.setName(results.getString("name"));

        return department;
    }

    @Override
    public List<Department> searchDepartmentsByName(String nameSearch) {

        nameSearch = "%" + nameSearch + "%";
        List<Department> departmentsByName = new ArrayList<>();    //create empty list to hold results

        String sqlSearchDepartmentsByName = "SELECT department_id, name " +    //write our query
                "FROM department " +
                "WHERE name LIKE ?";;
        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearchDepartmentsByName, nameSearch);    //executing the query and getting back a list of records

        while (results.next()) {
            Department department = mapRowToDepartment(results);
            departmentsByName.add(department);
        }

        return departmentsByName;
    }

    @Override
    public void saveDepartment(Department updatedDepartment) {

        String sqlSaveDepartment = "INSERT INTO department(department_id, name) " +    //define our query
                "VALUES(?, ?)";
        updatedDepartment.setId(getNextDepartmentId());	//calling a private method to grab next item in the sequence and set on newCity object
        jdbcTemplate.update(sqlSaveDepartment, updatedDepartment.getId(),
                updatedDepartment.getName());
    }

    private long getNextDepartmentId() {
        SqlRowSet nextDepartmentResult = jdbcTemplate.queryForRowSet("SELECT nextval('department_department_id_seq')");
        if (nextDepartmentResult.next()) {
            return nextDepartmentResult.getLong(1); // Is there a better way b/c 1's is a no no.??
        } else {
            throw new RuntimeException("Something went wrong while getting an id for the new department");
        }
    }


    @Override
    public Department createDepartment(Department newDepartment) {

        String newDepartmentSQL = "INSERT INTO department (department_id, name) " +
                "VALUES(?,?)";
        newDepartment.setId(getNextDepartmentId());
        jdbcTemplate.update(newDepartmentSQL,
                newDepartment.getId(),
                newDepartment.getName());

        return newDepartment;
    }

    @Override
    public Department getDepartmentById(Long id) {

        Department theDepartment = null;
        String sqlGetDepartmentById = "SELECT department_id, name " +    //select all rows
                "FROM department " +
                "WHERE id = ?";
        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetDepartmentById, id);
        if (results.next()) {
            theDepartment = mapRowToDepartment(results);
        }
        return theDepartment;
    }
}



