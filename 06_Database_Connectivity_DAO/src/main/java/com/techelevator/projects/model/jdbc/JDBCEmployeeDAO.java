package com.techelevator.projects.model.jdbc;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.sql.DataSource;

import com.techelevator.projects.model.Department;
import org.springframework.jdbc.core.JdbcTemplate;

import com.techelevator.projects.model.Employee;
import com.techelevator.projects.model.EmployeeDAO;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class JDBCEmployeeDAO implements EmployeeDAO {

	private JdbcTemplate jdbcTemplate;	//instance variable interacts with database

	public JDBCEmployeeDAO(DataSource dataSource) {	//constructor takes in datasource as argument

		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Employee> getAllEmployees() {

		List<Employee> allEmployees = new ArrayList<>();    //create empty list to hold results

		String sqlGetAllEmployees = "SELECT employee_id, department_id, first_name, last_name, birth_date, hire_date " +    //write our query
				"FROM employee";

		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllEmployees);    //executing the query and getting back a list of records

		while (results.next()) {    //loop through all records

			Employee employee = mapRowToEmployees(results);    //map each record to an employee object
			allEmployees.add(employee);    //add the department object to the list

		}

		return allEmployees;
	}

	private Employee mapRowToEmployees(SqlRowSet results) {
		Employee employee = new Employee();

		employee.setId(results.getLong("employee_id"));
		employee.setDepartmentId(results.getLong("department_id"));
		employee.setFirstName(results.getString("first_name"));
		employee.setLastName(results.getString("last_name"));
		employee.setBirthDay(results.getDate("birth_date").toLocalDate());
		employee.setHireDate(results.getDate("hire_date").toLocalDate());

		return employee;
	}

	@Override
	public List<Employee> searchEmployeesByName(String firstNameSearch, String lastNameSearch) {

		firstNameSearch = "%" + firstNameSearch + "%";
		lastNameSearch = "%" + lastNameSearch + "%";
		List<Employee> employeesByName = new ArrayList<>();    //create empty list to hold results

		String sqlSearchEmployeesByName = "SELECT employee_id, department_id, first_name, last_name, birth_date, hire_date " +    //write our query
				"FROM employee " +
				"WHERE first_name LIKE ? AND last_name LIKE ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearchEmployeesByName, firstNameSearch, lastNameSearch);    //executing the query and getting back a list of records

		while (results.next()) {
			Employee employee = mapRowToEmployees(results);
			employeesByName.add(employee);
		}

		return employeesByName;
	}

	@Override
	public List<Employee> getEmployeesByDepartmentId(long id) {

		List<Employee> employeesByDepartmentId = new ArrayList<>();    //create empty list to hold results

		String sqlGetEmployeesByDepartmentId = "SELECT employee_id, department_id, first_name, last_name, birth_date, hire_date " +    //select all rows
				"FROM employee " +
				"WHERE department_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetEmployeesByDepartmentId, id);
		if (results.next()) {
			Employee employee = mapRowToEmployees(results);
			employeesByDepartmentId.add(employee);
		}
		return employeesByDepartmentId;
	}



	@Override
	public List<Employee> getEmployeesWithoutProjects() {

		List<Employee> employeesWithoutProjects = new ArrayList<>();    //create empty list to hold results

		String sqlGetEmployeesWithoutProjects = "SELECT employee.employee_id, employee.department_id,first_name,last_name,birth_date,hire_date " +
				"FROM employee "+
				"LEFT JOIN project_employee ON project_employee.employee_id=employee.employee_id " +
				"WHERE project_employee.employee_id IS NULL" ;

		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetEmployeesWithoutProjects);
		if (results.next()) {
			Employee employee = mapRowToEmployees(results);
			employeesWithoutProjects.add(employee);
		}
		return employeesWithoutProjects;
	}

	@Override
	public List<Employee> getEmployeesByProjectId(Long projectId) {

		List<Employee> employeesByProjectId = new ArrayList<>();    //create empty list to hold results

<<<<<<< HEAD
		String sqlGetEmployeesByProjectId = "SELECT employee.department_id, employee.first_name, employee.last_name, employee.birth_date, employee.hire_date " +    //select all rows
=======
		String sqlGetEmployeesByProjectId = "SELECT project.project_id, employee.employee_id, employee.department_id, first_name, last_name, birth_date, hire_date " +    //select all rows

		String sqlGetEmployeesWithoutProjects = "SELECT employee.department_id, employee.first_name, employee.last_name, employee.birth_date, employee.hire_date " +    //select all rows
>>>>>>> cdab1b3676d02c957314eb5b8cdac9f544a0b533
				"FROM employee " +
				"JOIN project_employee ON employee.employee_id = project_employee.employee_id " +
				"JOIN project ON project_employee.project_id = project.project_id " +
				"WHERE project.project_id= ?";

		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetEmployeesByProjectId, projectId);
		if (results.next()) {
			Employee employee = mapRowToEmployees(results);
			employeesByProjectId.add(employee);
		}
		return employeesByProjectId;
	}

	@Override
	public void changeEmployeeDepartment(Long employeeId, Long departmentId) {

		String sqlChangeEmployeeDepartment = "UPDATE employee SET department_id = ?" +
				"WHERE employee_id = ?; ";
		jdbcTemplate.update(sqlChangeEmployeeDepartment, departmentId, employeeId);
		
	}

}
