package com.techelevator.projects.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.techelevator.projects.model.Employee;
import org.springframework.jdbc.core.JdbcTemplate;

import com.techelevator.projects.model.Project;
import com.techelevator.projects.model.ProjectDAO;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class JDBCProjectDAO implements ProjectDAO {

	private JdbcTemplate jdbcTemplate;

	public JDBCProjectDAO(DataSource dataSource) {

		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Project> getAllActiveProjects() {

		List<Project> allActiveProjects = new ArrayList<>();    //create empty list to hold results

		String sqlGetAllActiveProjects = "SELECT project_id, name, from_date, to_date " +    //write our query
				"FROM project " +
				"WHERE project.from_date IS NOT NULL";

		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllActiveProjects);    //executing the query and getting back a list of records

		while (results.next()) {    //loop through all records
			if (sqlGetAllActiveProjects != null) {
				Project project = mapRowToProjects(results);    //map each record to an employee object
				allActiveProjects.add(project);    //add the department object to the list
			}
		}

		return allActiveProjects;
	}

	private Project mapRowToProjects(SqlRowSet results) {
		Project project = new Project();

		project.setId(results.getLong("project_id"));
		project.setName(results.getString("name"));
		project.setStartDate(results.getDate("from_date").toLocalDate());
		project.setEndDate(results.getDate("to_date").toLocalDate());

		return project;
	}

	@Override
	public void removeEmployeeFromProject(Long projectId, Long employeeId) {

		jdbcTemplate.update("DELETE FROM ? WHERE employee = ?");

	}

	@Override
	public void addEmployeeToProject(Long projectId, Long employeeId) {

		String newProjectSQL = "INSERT INTO project (project_id, employee_id) " +
				"VALUES(?,?)";

		jdbcTemplate.update(newProjectSQL,
				projectId,
				employeeId);

	}

}
