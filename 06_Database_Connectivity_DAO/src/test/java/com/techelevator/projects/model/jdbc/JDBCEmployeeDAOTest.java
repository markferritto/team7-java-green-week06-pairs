package com.techelevator.projects.model.jdbc;

import org.junit.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import java.sql.SQLException;

import static org.junit.Assert.*;

public class JDBCEmployeeDAOTest {

    private static final String TEST_EMPLOYEE = "XYZ";

    private static SingleConnectionDataSource dataSource;
    private JDBCEmployeeDAO dao;

    @BeforeClass
    public static void setupDataSource() {
        dataSource = new SingleConnectionDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/projects");	//change source, copy everything else
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres1");
        /* The following line disables autocommit for connections
         * returned by this DataSource. This allows us to rollback
         * any changes after each test */
        dataSource.setAutoCommit(false);
    }

    @AfterClass
    public static void closeDataSource() throws SQLException {
        dataSource.destroy();
    }

    @Before
    public void setUp() throws Exception {
        String sqlInsertEmployee = "INSERT INTO employee (?, department_id, first_name, last_name, birth_date, hire_date) VALUES (?, YYZ, Dennis, Rodman, 1961-05-13, 1986-06-17)";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update(sqlInsertEmployee);
        dao = new JDBCEmployeeDAO(dataSource);
    }

    @After
    public void rollback() throws SQLException {
        dataSource.getConnection().rollback();
    }

    @Test
    public void return_employees_by_department_id() {

    }

    @Test
    public void return_employees_without_projects() {

    }


}