package com.techelevator.projects.model.jdbc;

import com.techelevator.projects.model.Project;
import org.junit.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;

public class JDBCProjectDAOTest {

    private static final String TEST_PROJECT = "XYZ";

    private static SingleConnectionDataSource dataSource;
    private JDBCEmployeeDAO dao;

    @BeforeClass
    public static void setupDataSource() {
        dataSource = new SingleConnectionDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/projects");	//change source, copy everything else
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres1");
        /* The following line disables autocommit for connections
         * returned by this DataSource. This allows us to rollback
         * any changes after each test */
        dataSource.setAutoCommit(false);
    }

    @AfterClass
    public static void closeDataSource() throws SQLException {
        dataSource.destroy();
    }

    @Before
    public void setUp() throws Exception {
        String sqlInsertProject = "INSERT INTO project (?, name, from_date, to_date) VALUES (?, Chicago Bulls, 1961-05-13, 1986-06-17)";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update(sqlInsertProject);
        dao = new JDBCEmployeeDAO(dataSource);
    }

    @After
    public void rollback() throws SQLException {
        dataSource.getConnection().rollback();
    }

    @Test
    public void delete_employee_from_projects() {

    }

    @Test
    public void add_employee_to_projects() {

    }

}