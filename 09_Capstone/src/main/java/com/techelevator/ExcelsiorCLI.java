package com.techelevator;

import javax.sql.DataSource;

import com.techelevator.dao.ReservationDAO;
import com.techelevator.dao.SpaceDAO;
import com.techelevator.dao.VenueDAO;
import com.techelevator.jdbc.JDBCReservationDAO;
import com.techelevator.jdbc.JDBCSpaceDAO;
import com.techelevator.jdbc.JDBCVenueDAO;
import com.techelevator.main.Reservation;
import com.techelevator.main.Space;
import com.techelevator.main.Venue;
import com.techelevator.view.Menu;
import org.apache.commons.dbcp2.BasicDataSource;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class ExcelsiorCLI {

    private Menu menu;
    private VenueDAO venueData;
    private Venue selectedVenue = new Venue();
    private SpaceDAO spaceData;
    private Space selectedSpace = new Space();
    private ReservationDAO reservationData;

    public static void main(String[] args) {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/excelsior_venues");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres1");

        ExcelsiorCLI application = new ExcelsiorCLI(dataSource);
        application.run();
    }

    public ExcelsiorCLI(DataSource datasource) {
        this.menu = new Menu();
        this.venueData = new JDBCVenueDAO(datasource);
        this.spaceData = new JDBCSpaceDAO(datasource);
        this.reservationData = new JDBCReservationDAO(datasource);
    }

    public void run() {

        boolean running = true;

        while (running) {

            String mainMenuResponse = menu.printMainMenu();

            if (mainMenuResponse.equals("1")) {

                List<Venue> listAllVenues = venueData.getAllVenues();




                boolean venueMenuRunning = true;
                {
                    while (venueMenuRunning) {

                        int selectedVenueNumber = Integer.parseInt(menu.printListOfAllVenues(listAllVenues));
                        Venue selectedVenueFromList = listAllVenues.get(selectedVenueNumber - 1);

                        String userVenueDetailsResponse = menu.printVenueDetails(selectedVenueFromList);
                        if (userVenueDetailsResponse.equals("1")) ;



                        boolean spacesMenuRunning = true;
                        {
                            while (spacesMenuRunning) {

                                List<Space> listOfSpaces = spaceData.getAllVenueSpaces(selectedVenueFromList.getId());

                                menu.printListOfVenueSpacesHeader(selectedVenueFromList);
                                String userSpaceResponse = menu.printListOfVenueSpaces(listOfSpaces);
                                if (userSpaceResponse.equals("1")) ;





                                boolean reservationSpaceMenuRunning = true;
                                {
                                    while (reservationSpaceMenuRunning) {

                                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");  //Arrival Date
                                        LocalDate userArrivalDate = LocalDate.parse(menu.printSelectReservationDate(), formatter);

                                        int amountOfDaysInSpace = Integer.parseInt(menu.printAmountOfDaysForSpace());
                                        int amountOfPeopleAttending = Integer.parseInt(menu.printAmountOfPeopleAttending());

                                        LocalDate userDepartureDate = userArrivalDate.plusDays(amountOfDaysInSpace - 1);


                                        //While loop needed to resape peopleAttending pint Q's
                                        List<Space> topFiveListOfSpaces = reservationData.getTopFiveReservations(selectedVenueNumber, amountOfPeopleAttending, userArrivalDate, userDepartureDate);

                                        menu.printTopFiveAvailableSpaces(topFiveListOfSpaces);

                                        //BigDecimal totalCost = BigDecimal.printTopFiveAvailableSpaces();
                                        int whichSpaceToReserve = Integer.parseInt(menu.printWhichSpaceToReserve());
                                        Space selectedSpaceFromList = topFiveListOfSpaces.get(whichSpaceToReserve);
                                        String whoReservationFor = menu.printWhoReservationFor();

                                        menu.printConfirmationReport(selectedVenueFromList, selectedSpaceFromList, whoReservationFor, amountOfPeopleAttending, userArrivalDate, userDepartureDate);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}