package com.techelevator.dao;

import com.techelevator.main.Reservation;
import com.techelevator.main.Space;

import java.time.LocalDate;
import java.util.List;

public interface ReservationDAO {

    // move this 2 the space DAO
    public List<Space> getTopFiveReservations(int selectedVenueId, int amountOfPeopleAttending,  LocalDate userArrivalDate, LocalDate userDepartureDate);
    public void createReservation (int id, LocalDate userArrivalDate, LocalDate userDepartureDate, int amountOfPeopleAttending);// pass in as a Rev Object, HAve this return the object(to print out)

}
