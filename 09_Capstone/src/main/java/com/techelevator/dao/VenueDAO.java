package com.techelevator.dao;

import com.techelevator.main.Venue;

import java.util.List;

public interface VenueDAO {

    public List<Venue> getAllVenues();

}
