package com.techelevator.dao;

import com.techelevator.main.Space;

import java.util.List;

public interface SpaceDAO {

    public List<Space> getAllVenueSpaces(int venueId);

}
