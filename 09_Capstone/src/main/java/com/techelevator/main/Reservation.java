package com.techelevator.main;

import java.math.BigDecimal;
import java.time.LocalDate;



public class Reservation {

    private int id;
    private int spaceId;
    private int numberOfAttendees;
    private LocalDate startDate;
    private LocalDate endDate;
    private String reservationName;

    public Reservation() {

    }



    public Reservation (int id, int spaceId, int numberOfAttendees, LocalDate startDate, LocalDate endDate, String reservationName) {
        this.id = id;
        this.spaceId = spaceId;
        this.numberOfAttendees = numberOfAttendees;
        this.startDate = startDate;
        this.endDate = endDate;
        this.reservationName = reservationName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSpaceId() {
        return spaceId;
    }

    public void setSpaceId(int spaceId) {
        this.spaceId = spaceId;
    }

    public int getNumberOfAttendees() {
        return numberOfAttendees;
    }

    public void setNumberOfAttendees(int numberOfAttendees) {
        this.numberOfAttendees = numberOfAttendees;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getReservationName() {
        return reservationName;
    }

    public void setReservationName(String reservationName) {
        this.reservationName = reservationName;
    }
}
