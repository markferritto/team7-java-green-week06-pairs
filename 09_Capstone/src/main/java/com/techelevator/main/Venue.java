package com.techelevator.main;

import java.util.List;

public class Venue {

    private int id;
    private int cityId;
    private String name;
    private String city;
    private String state;
    private List<String> categories;
    private String description;

    public Venue () {

    }

    public Venue (int id, String name, String city, String state, List<String> categories, String description) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.state = state;
        this.categories = categories;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public List<String> getCategories() {
        return categories;
    }

    public String getDescription() {
        return description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }
}
