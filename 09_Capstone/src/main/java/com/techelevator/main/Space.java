package com.techelevator.main;

import java.math.BigDecimal;

public class Space {

    private long id;
    private String name;
    private int openFrom;
    private int openTo;
    private BigDecimal dailyRate;
    private boolean isAccessible;
    private int maxOccupancy;

    public Space () {

    }

    public Space (long id, String name, int openFrom, int openTo, BigDecimal dailyRate, boolean isAccessible, int maxOccupancy) {
        this.id = id;
        this.name = name;
        this.openFrom = openFrom;
        this.openTo = openTo;
        this.dailyRate = dailyRate;
        this.isAccessible = isAccessible;
        this.maxOccupancy = maxOccupancy;
    }

    public int getMaxOccupancy() {
        return maxOccupancy;
    }

    public void setMaxOccupancy(int maxOccupancy) {
        this.maxOccupancy = maxOccupancy;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOpenFrom() {

        return openFrom;
    }

    public void setOpenFrom(int openFrom) {
        this.openFrom = openFrom;
    }

    public int getOpenTo() {

        return openTo;
    }

    public void setOpenTo(int openTo) {
        this.openTo = openTo;
    }

    public BigDecimal getDailyRate() {
        return dailyRate;
    }

    public void setDailyRate(BigDecimal dailyRate) {
        this.dailyRate = dailyRate;
    }

    public boolean isAccessible() {
        return isAccessible;
    }

    public void setAccessible(boolean accessible) {
        isAccessible = accessible;
    }

    public String intToMonth(int month) {  // Use this to Change the month #'s into Strings
        String months= "";
        if (month==12) {
            months = "Dec.";
        } if(month==11) {
            months = "Nov.";
        } if(month==10) {
            months = "Oct.";
        } if(month==9) {
            months = "Sep.";
        } if(month==8) {
            months = "Aug.";
        } if(month==7) {
            months = "Jul.";
        } if(month==6) {
            months = "Jun.";
        } if(month==5) {
            months = "May.";
        } if(month==4) {
            months = "Apr.";
        } if(month==3) {
            months = "Mar.";
        } if(month==2) {
            months = "Feb.";
        } if(month==1) {
            months = "Jan.";
        }
        return months;
    }

}
