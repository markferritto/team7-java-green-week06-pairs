package com.techelevator.view;

import com.techelevator.main.Reservation;
import com.techelevator.main.Space;
import com.techelevator.main.Venue;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

public class Menu {

    private Scanner scanner;

    public Menu() {

        scanner = new Scanner(System.in);

    }

    public String printMainMenu() {
        System.out.println("***************************");
        System.out.println("Java Green Venue App 3000");
        System.out.println("***************************\n");

        System.out.println("What would you like to do?");
        System.out.println("1) List Venues");
        System.out.println("Q) Quit\n");

        System.out.println("Please select your choice: ");

        return scanner.nextLine();
    }

    public String printListOfAllVenues(List<Venue> listOfVenues) {

        System.out.println("Which venue would you like to view? (please enter only the number)");

        int venueNumber = 0;

        for (int i = 0; i < listOfVenues.size(); i++) {
            venueNumber++;

            System.out.println(venueNumber + ") " + listOfVenues.get(i).getName());
        }

        System.out.println("R) Return to Previous Screen");

        String userVenueResponse = scanner.nextLine();

        return userVenueResponse;
    }

    public String printVenueDetails(Venue selectedVenueFromList) {

        System.out.println(selectedVenueFromList.getName());
        System.out.println("-------------");
        System.out.printf("Categories: %s\n", selectedVenueFromList.getCategories());
        System.out.printf("Location: %s\n", selectedVenueFromList.getCity() + ", " + selectedVenueFromList.getState());
        System.out.println();
        System.out.printf("%s\n", selectedVenueFromList.getDescription());
        System.out.println();
        System.out.println("What would you like to do next?? (please enter only the number or letter)");
        System.out.println("1) View Spaces");
        System.out.println("2) Search for Reservation");
        System.out.println("R) Return to Previous Screen\n");

        String userVenueDetailsResponse = scanner.nextLine();

        return userVenueDetailsResponse;
    }

    public void printListOfVenueSpacesHeader(Venue selectedVenueFromList) {

        System.out.println(selectedVenueFromList.getName() + " Spaces\n");

        System.out.printf("%7s %25s %25s %25s %25s %n", "Name", "Open", "Close", "Daily Rate", "Max Occupancy");
    }

    public String printListOfVenueSpaces(List<Space> listOfSpaces) {

        int spaceNumber = 0;

        for (int i = 0; i < listOfSpaces.size(); i++) {
            spaceNumber++;
            System.out.printf("%-25s %-25s %-25s %-25s %-25s %n", spaceNumber + ") " + listOfSpaces.get(i).getName(), listOfSpaces.get(i).getOpenFrom(), listOfSpaces.get(i).getOpenTo(), listOfSpaces.get(i).getDailyRate(), listOfSpaces.get(i).getMaxOccupancy());


        }

        System.out.println("What would you like to do next?? (please enter only the number or letter)");
        System.out.println("1) Reserve a Space");
        System.out.println("R) Return to Previous Screen\n");

        String userSpaceResponse = scanner.nextLine();

        return userSpaceResponse;

    }

    public String printSelectReservationDate() {

        System.out.println("When do you need the space? MM/DD/YYYY: ");
        return scanner.nextLine();
    }

    public String printAmountOfDaysForSpace() {

        System.out.println("How many days will you need the space? ");
        return scanner.nextLine();
    }

    public String printAmountOfPeopleAttending() {

        System.out.println("How many people will be in attendance? ");

        return scanner.nextLine();
    }

    public void printTopFiveAvailableSpaces(List<Space> topFiveListOfSpaces) {

        System.out.println("The following spaces are available based on your needs: ");

        System.out.printf("%7s %25s %25s %25s %25s %n", "Space #", "Name", "Daily Rate", "Accessible?", "Total Cost");

        for (Space topFive : topFiveListOfSpaces) {

//            int amountOfPeopleAttending = Integer.parseInt(printAmountOfPeopleAttending());
            BigDecimal totalCost = topFive.getDailyRate().multiply(BigDecimal.valueOf(Integer.parseInt(printAmountOfPeopleAttending())));

            System.out.printf("%-25s %-25s %-25s %-25s %-25s %n", topFive.getId(), topFive.getName(), topFive.getDailyRate(), topFive.isAccessible(), totalCost);

        }
    }

    public String printWhichSpaceToReserve() {

        System.out.println("Which space would you like to reserve (enter 0 to cancel)? ");
        return scanner.nextLine();
    }

    public String printWhoReservationFor() {

        System.out.println("Who is this reservation for? ");
        return scanner.nextLine();
    }

    public void printConfirmationReport(Venue selectedVenueFromList, Space selectedSpaceFromList, String whoReservationFor,  int amountOfPeopleAttending, LocalDate userArrivalDate, LocalDate userDepartureDate) {

        System.out.println("Thanks for submitting your reservation! The details for your event are listed below: ");

        System.out.println("Confirmation #: 69696969 ");
        System.out.printf("Venue: %s\n", selectedVenueFromList.getName());
        System.out.printf("Space: %s\n", selectedSpaceFromList.getName());
        System.out.printf("Reserved For: %s\n", whoReservationFor);
        System.out.printf("Attendees: %s\n", amountOfPeopleAttending);
        System.out.printf("Arrival Date: %s\n", userArrivalDate);
        System.out.printf("Depart Date: %s\n", userDepartureDate);
        //System.out.printf("Total Cost: %s\n", );
    }

}
