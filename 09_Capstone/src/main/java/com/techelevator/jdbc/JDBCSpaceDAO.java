package com.techelevator.jdbc;

import com.techelevator.dao.SpaceDAO;
import com.techelevator.main.Space;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

public class JDBCSpaceDAO implements SpaceDAO {

    private JdbcTemplate jdbcTemplate;

    public JDBCSpaceDAO(DataSource datasource) {

        this.jdbcTemplate = new JdbcTemplate(datasource);

    }

    @Override
    public List<Space> getAllVenueSpaces(int venueId) {

        List<Space> newSpacesList = new ArrayList<>();

        String sqlGetAllVenueSpaces = "SELECT space.id, space.name, space.is_accessible, space.open_from, space.open_to, CAST (daily_rate AS decimal), space.max_occupancy " +
                "FROM space " +
                "JOIN venue ON venue.id = space.venue_id " +
                "WHERE venue.id = ?" +
                "ORDER BY space.name";

        Long id = (long) venueId;

        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllVenueSpaces, id);

        while (results.next()) {
            Space space = mapRowToSpace(results);
            newSpacesList.add(space);
        }

        return newSpacesList;
    }



    //Same as Venue and Setting the the name from the SQL Statements
    private Space mapRowToSpace(SqlRowSet results) {

        Space mappedVenueSpace = new Space();

        mappedVenueSpace.setName(results.getString("name"));
        mappedVenueSpace.setId(results.getLong("id"));
        mappedVenueSpace.setOpenFrom(results.getInt("open_from"));
        mappedVenueSpace.setOpenTo(results.getInt("open_to"));
        mappedVenueSpace.setDailyRate(results.getBigDecimal("daily_rate"));
        mappedVenueSpace.setMaxOccupancy(results.getInt("max_occupancy"));
        mappedVenueSpace.setAccessible(results.getBoolean("is_accessible"));

        return mappedVenueSpace;
    }

}
