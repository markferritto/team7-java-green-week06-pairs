package com.techelevator.jdbc;

import com.techelevator.dao.VenueDAO;
import com.techelevator.main.Venue;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class JDBCVenueDAO implements VenueDAO {

    private JdbcTemplate jdbcTemplate;

    public JDBCVenueDAO(DataSource datasource) {

        this.jdbcTemplate = new JdbcTemplate(datasource);

    }


    @Override
    public List<Venue> getAllVenues() {


        List<Venue> newVenueList = new ArrayList<>();

        String sqlGetAllVenues = "Select distinct venue.name as venue_name, venue.id as venue_id, city.id as city_id, city.name as city_name, city.state_abbreviation as state_name, venue.description as venue_description " +
                "FROM venue " +
                "full outer JOIN city on venue.city_id = city.id " +
                "full outer JOIN state on city.state_abbreviation = state.abbreviation " +
                "full outer JOIN category_venue on venue.id = category_venue.venue_id " +
                "full outer JOIN category on category_venue.category_id = category.id " +
                "full outer JOIN space on space.venue_id = venue.id " +
                "full outer JOIN reservation on reservation.space_id = space.id ";

        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllVenues);

        while (results.next()) {
            Venue venue = mapRowToVenue(results);
            newVenueList.add(venue);
        }

        return newVenueList;
    }





    private Venue mapRowToVenue(SqlRowSet results) {

        Venue mappedVenue = new Venue();

        mappedVenue.setName(results.getString("venue_name"));
        mappedVenue.setId(results.getInt("venue_id"));
        mappedVenue.setCityId(results.getInt("city_id"));
        mappedVenue.setCity(results.getString("city_name"));
        mappedVenue.setState(results.getString("state_name"));
        mappedVenue.setDescription(results.getString("venue_description"));

        List<String> categoryList = new ArrayList<>();

        String sqlGetCategories = "Select category.name " +
                "FROM venue " +
                "full outer JOIN category_venue on venue.id = category_venue.venue_id " +
                "full outer JOIN category on category_venue.category_id = category.id " +
                "WHERE venue.name = ?";

        SqlRowSet categoryResults = jdbcTemplate.queryForRowSet(sqlGetCategories, mappedVenue.getName());

        while (categoryResults.next()) {
            String category = categoryResults.getString("name");
            categoryList.add(category);
        }

        mappedVenue.setCategories(categoryList);
        return mappedVenue;
    }

}
