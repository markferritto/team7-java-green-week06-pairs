package com.techelevator.jdbc;

import com.techelevator.dao.ReservationDAO;
import com.techelevator.main.Reservation;

import com.techelevator.main.Space;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class JDBCReservationDAO implements ReservationDAO {

    private JdbcTemplate jdbcTemplate;

    public JDBCReservationDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Space> getTopFiveReservations(int selectedVenueId, int amountOfPeopleAttending,  LocalDate userArrivalDate, LocalDate userDepartureDate) {

        List<Space> newReservationSpaceList = new ArrayList<>();

        String sqlRetrieveAvailableSpaces = "SELECT space.id, space.venue_id, space.name, space.is_accessible, space.open_from, space.open_to, CAST(space.daily_rate AS decimal), space.max_occupancy FROM space " +
                "JOIN venue ON venue.id = space.venue_id " +
                "WHERE venue_id = ? " +
                "AND max_occupancy >= ? " +
                "AND NOT EXISTS (SELECT * FROM reservation " +
                "WHERE (CAST(? AS DATE) BETWEEN reservation.start_date AND reservation.end_date " +
                "OR CAST(? AS DATE) BETWEEN reservation.start_date AND reservation.end_date) " +
                "AND reservation.space_id = space.id) " +
                "AND ((EXTRACT(MONTH from CAST(? AS DATE)) BETWEEN space.open_from AND space.open_to) OR space.open_from IS NULL AND space.open_to IS NULL) " +
                "AND ((EXTRACT(MONTH from CAST(? AS DATE)) BETWEEN space.open_from AND space.open_to) OR space.open_from IS NULL AND space.open_to IS NULL) " +
                "GROUP BY space.id " +
                "ORDER BY space.daily_rate ASC " +
                "LIMIT 5";

        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlRetrieveAvailableSpaces, selectedVenueId, amountOfPeopleAttending, userArrivalDate, userDepartureDate, userArrivalDate, userDepartureDate);

        while (results.next()) {
            Space reservationSpace = mapRowToSpaceReservation(results); //Should be Space object, looking for available spaces

            newReservationSpaceList.add(reservationSpace);
        }

        return newReservationSpaceList;
    }


    //Getting SQL results
    private Space mapRowToSpaceReservation(SqlRowSet results) {
        Space mappedSpaceReservation = new Space();

        mappedSpaceReservation.setId(results.getInt("id"));
        mappedSpaceReservation.setName(results.getString("name"));
        mappedSpaceReservation.setAccessible(results.getBoolean("is_accessible"));
        mappedSpaceReservation.setOpenFrom(results.getInt("open_from"));
        mappedSpaceReservation.setOpenTo(results.getInt("open_to"));
        mappedSpaceReservation.setDailyRate(results.getBigDecimal("daily_rate"));
        mappedSpaceReservation.setMaxOccupancy(results.getInt("max_occupancy"));

        return mappedSpaceReservation;
    }

    @Override
    public void createReservation(int id, LocalDate userArrivalDate, LocalDate userDepartureDate, int amountOfPeopleAttending) {
        String reservation = ("INSERT INTO reservation (id, userArrivalDate, userDepartureDate, amountOfPeopleAttending) VALUES (?,?,?,?)");
                jdbcTemplate.update(reservation, id, userArrivalDate, userDepartureDate, amountOfPeopleAttending);
    }

//    private Reservation mapRowToReservationId(SqlRowSet reservationNextRow) {
//        Reservation reservation = new Reservation();
//        reservation.setId(reservationNextRow.getInt("reservation_id"));
//        return reservation;
//    }

}
