package com.techelevator.jdbc;

import java.sql.SQLException;

import javax.sql.DataSource;

import com.techelevator.jdbc.JDBCSpaceDAO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

public class JDBCSpaceDAOTest {


    /*
     * Using this particular implementation of DataSource so that every database
     * interaction is part of the same database session and hence the same database
     * transaction
     */
    private static SingleConnectionDataSource dataSource;
    private JDBCSpaceDAO dao;
    /*
     * Before any tests are run, this method initializes the datasource for testing.
     */
    @BeforeClass
    public static void setupDataSource() {
        dataSource = new SingleConnectionDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/excelsior_venues");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres1");
        /*
         * The following line disables autocommit for connections returned by this
         * DataSource. This allows us to rollback any changes after each test
         */
        dataSource.setAutoCommit(false);
    }

    /*
     * After all tests have finished running, this method will close the DataSource
     */

    @After
    public void rollback() throws SQLException {
        dataSource.getConnection().rollback();
    }


    @AfterClass
    public static void closeDataSource() throws SQLException {
        dataSource.destroy();
    }

    @Test
    public void getAllVenueSpaces() {
    }
}