package com.techelevator.jdbc;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import static org.junit.Assert.*;

public class JDBCReservationDAOTest {

    private static SingleConnectionDataSource dataSource;
    private JDBCReservationDAO dao;

    @BeforeClass
    public static void setupDataSource() { //copy all of this and change world on 36
        dataSource = new SingleConnectionDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/world");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres1");
        /* The following line disables autocommit for connections
         * returned by this DataSource. This allows us to rollback
         * any changes after each test */
        dataSource.setAutoCommit(false);
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void rollback() throws SQLException {
        dataSource.getConnection().rollback();
    }


    @AfterClass
    public static void closeDataSource() throws SQLException {
        dataSource.destroy();
    }

    @Test
    public void getTopFiveReservations() {
    }

    @Test
    public void createReservation() {
    }
}