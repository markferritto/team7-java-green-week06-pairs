package com.techelevator.jdbc;

import org.junit.*;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import java.sql.SQLException;

public class JDBCVenueDAOTest {

    private static final String TEST_VENUE = "XYZ";


    private static SingleConnectionDataSource dataSource;
    private JDBCSpaceDAO dao;

    @BeforeClass
    public static void setupDataSource() { //copy all of this and change world on 36
        dataSource = new SingleConnectionDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/world");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres1");
        /* The following line disables autocommit for connections
         * returned by this DataSource. This allows us to rollback
         * any changes after each test */
        dataSource.setAutoCommit(false);
    }

    @Before
    public void setUp() throws Exception {
    }

//    @Before
//    public void setup() {  // this is part of the arrange message!
//        String sqlInsertCountry = "INSERT INTO country (code, name, continent, region, surfacearea, indepyear, population, lifeexpectancy, gnp, gnpold, localname, governmentform, headofstate, capital, code2) VALUES (?, 'Afghanistan', 'Asia', 'Southern and Central Asia', 652090, 1919, 22720000, 45.9000015, 5976.00, NULL, 'Afganistan/Afqanestan', 'Islamic Emirate', 'Mohammad Omar', 1, 'AF')";
//        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
//        jdbcTemplate.update(sqlInsertCountry, TEST_COUNTRY);  //USE THE CODE FRO DAO 53-55  INSERT SOMETHING
//        dao = new JDBCCityDAO(dataSource);
//    }

    @After
    public void rollback() throws SQLException {
        dataSource.getConnection().rollback();
    }


    @AfterClass
    public static void closeDataSource() throws SQLException {
        dataSource.destroy();
    }

    @Test
    public void getAllVenues() {
    }
}