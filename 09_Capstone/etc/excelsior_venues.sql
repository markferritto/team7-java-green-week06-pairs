SELECT venue.id, venue.name
FROM venue
ORDER BY venue.name ASC
--Group by venue.name;



--List Venue Spaces

select DISTINCT venue.name as venue_name, space.name as space_name, space.open_from as Open, space.open_to as Close, space.Daily_Rate, space.Max_Occupancy
FROM venue
full outer JOIN city on venue.city_id = city.id
full outer JOIN state on city.state_abbreviation = state.abbreviation
full outer JOIN category_venue on venue.id = category_venue.venue_id
full outer JOIN category on category_venue.category_id = category.id
full outer JOIN space on space.venue_id = venue.id
full outer JOIN reservation on reservation.space_id = space.id
order by venue.name
--Group by space.name, space.open_from, space.open_to, space.daily_rate, space.max_occupancy;



-- Venue Detail
--get VenueBY NAME

Select venue.name as venue_name , city.name as City_Name, city.state_abbreviation, category.name as category_name, venue.description 
FROM venue
full outer JOIN city on venue.city_id = city.id
full outer JOIN state on city.state_abbreviation = state.abbreviation
full outer JOIN category_venue on venue.id = category_venue.venue_id
full outer JOIN category on category_venue.category_id = category.id
full outer JOIN space on space.venue_id = venue.id
full outer JOIN reservation on reservation.space_id = space.id
Group by venue.name, city.name, city.state_abbreviation, category.name, venue.description;





SELECT venue.name, city.name as city, state.abbreviation as State, category_venue.venue_id, category.name, space.daily_rate, reservation.space_id
FROM venue
full outer JOIN city on venue.city_id = city.id
full outer JOIN state on city.state_abbreviation = state.abbreviation
full outer JOIN category_venue on venue.id = category_venue.venue_id
full outer JOIN category on category_venue.category_id = category.id
full outer JOIN space on space.venue_id = venue.id
full outer JOIN reservation on reservation.space_id = space.id
Group by venue.name, city.name, state.abbreviation, category_venue.venue_id, category.name, space.daily_rate, reservation.space_id



--Reserve a Space
Select venue.name, space.daily_rate, space.max_occupancy, space.is_accessible as accessible
FROM venue
full outer JOIN city on venue.city_id = city.id
full outer JOIN state on city.state_abbreviation = state.abbreviation
full outer JOIN category_venue on venue.id = category_venue.venue_id
full outer JOIN category on category_venue.category_id = category.id
full outer JOIN space on space.venue_id = venue.id
full outer JOIN reservation on reservation.space_id = space.id
Group by venue.name, space.daily_rate, space.max_occupancy, space.is_accessible
-- ADD Total Cost 
--space.daily_rate * days = 




--trying out
Select  distinct venue.name as venue_name , city.name as City_Name, city.state_abbreviation, venue.description
FROM venue
full outer JOIN city on venue.city_id = city.id
full outer JOIN state on city.state_abbreviation = state.abbreviation
full outer JOIN category_venue on venue.id = category_venue.venue_id
full outer JOIN category on category_venue.category_id = category.id
full outer JOIN space on space.venue_id = venue.id
full outer JOIN reservation on reservation.space_id = space.id

--Group by venue.name, city.name, city.state_abbreviation, venue.description;


--venue ID, and category ID
Select  distinct category_venue.venue_id, venue.name as venue_name, category_venue.category_id, category.name as category_name
FROM venue
full outer JOIN category_venue on venue.id = category_venue.venue_id
full outer JOIN category on category_venue.category_id = category.id
order by category_venue.venue_id  

--category ID
select category_venue.category_id, category.name as category_name
FROM venue
full outer JOIN category_venue on venue.id = category_venue.venue_id
full outer JOIN category on category_venue.category_id = category.id
order by category_venue.category_id

--Venue ID 
Select  distinct category_venue.venue_id, venue.name as venue_name
FROM venue
full outer JOIN category_venue on venue.id = category_venue.venue_id
full outer JOIN category on category_venue.category_id = category.id
order by  venue.name ASC
--order by category_venue.venue_id  




--Reserve a Space 6/19/21

Select venue.name, space.daily_rate, space.max_occupancy, space.is_accessible as accessible
FROM venue
full outer JOIN city on venue.city_id = city.id
full outer JOIN space on space.venue_id = venue.id
Group by venue.name, space.daily_rate, space.max_occupancy, space.is_accessible
-- ADD Total Cost 
--space.daily_rate * days = 

--Reserve a this is just RESERVATION 6/19/21 Already in place
Select reservation.*
From reservation
where start_date >= end_date
where 
where = ?
-- have dates to string to Cast them in the Database
space.id, != ()

SELECT space.* , venue.id
From reservation
full outer join venue on space.venue_id = venue.id
where absolute (reservation.startDate, reservation.endDate) 
where -- space.id is a subquery 
FROM reservatio


SELECT space.*
FROM space
JOIN venue ON venue.id = space.venue_id
WHERE venue.id = ?
AND space.id NOT IN
(SELECT space_id
FROM reservation)
AND max_occupancy >= ?
GROUP
ORDER
LIMIT 5;


SELECT reservation.start_date, reservation.end_date,space.open_from, space.open_to
from reservation
join space on reservation.space_id = space.id
where reservation_id = venue_id and reservation_id = space_id






SELECT space.id, space.venue_id, space.name, space.is_accessible, space.open_from, space.open_to, CAST(space.daily_rate AS decimal), space.max_occupancy FROM space
                JOIN venue ON venue.id = space.venue_id 
                WHERE venue_id = ?
                AND max_occupancy >= ? 
                AND NOT EXISTS (SELECT * FROM reservation
                WHERE (CAST(? AS DATE) BETWEEN reservation.start_date AND reservation.end_date
                OR CAST(? AS DATE) BETWEEN reservation.start_date AND reservation.end_date) 
                AND reservation.space_id = space.id) 
                AND ((EXTRACT(MONTH from CAST(? AS DATE)) BETWEEN space.open_from AND space.open_to) OR space.open_from IS NULL AND space.open_to IS NULL) 
                AND ((EXTRACT(MONTH from CAST(? AS DATE)) BETWEEN space.open_from AND space.open_to) OR space.open_from IS NULL AND space.open_to IS NULL) 
                GROUP BY space.id 
                ORDER BY space.daily_rate ASC
                LIMIT 5


